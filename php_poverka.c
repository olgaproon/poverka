/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2017 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_poverka.h"
#include "poverka.h"

static zend_class_entry *calc_res_ptr;

PHP_FUNCTION(confirm_poverka_compiled)
{
	char *arg = NULL;
	size_t arg_len, len;
	zend_string *strg;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &arg, &arg_len) == FAILURE) {
		return;
	}

	strg = strpprintf(0, "Congratulations! You have successfully modified ext/%.78s/config.m4. Module %.78s is now compiled into PHP.", "poverka", arg);

	RETURN_STR(strg);
}

PHP_FUNCTION(poverka){
	int n_rows, n_front, n_1, heat_carrier;
	double V_air_out, t_air_in, fi_in, Rz, tw_in, tw_out, A, B, d, delta_wall, lambda_wall, lam_w,
    Ck, h_gofr, delta_f, b_lam, s1, s2, lambda_f, coef, d_col, d_col_ext, m_col, concentration;

	if (zend_parse_parameters(
		ZEND_NUM_ARGS() TSRMLS_CC,
		"ddddddddllldddddddddddddddld",
		&V_air_out, &t_air_in, &fi_in, &Rz, &tw_in, &tw_out, &A, &B, &n_rows, &n_front, &n_1,
		&d, &delta_wall, &lambda_wall, &lam_w, &Ck, &h_gofr, &delta_f, &b_lam, &s1, &s2, &lambda_f,
		&coef, &d_col, &d_col_ext, &m_col, &heat_carrier, &concentration
	) != SUCCESS){
		return;
	}
	struct calc result = poverka(
		V_air_out, t_air_in, fi_in, Rz, tw_in, tw_out, A, B, (int)n_rows, (int)n_front, (int)n_1,
		d, delta_wall, lambda_wall, lam_w, Ck, h_gofr, delta_f, b_lam, s1, s2,
		lambda_f, coef, d_col, d_col_ext, m_col, (int)heat_carrier, concentration
	);

	object_init_ex(return_value, calc_res_ptr);
	add_property_double(return_value, "Q", result.Q);
	add_property_double(return_value, "dzeta", result.dzeta);
	add_property_double(return_value, "t_air_out", result.t_air_out);
	add_property_double(return_value, "tp", result.tp);
	add_property_double(return_value, "fi_out", result.fi_out);
	add_property_double(return_value, "Gw", result.Gw);
	add_property_double(return_value, "w", result.w);
	add_property_double(return_value, "v", result.v);
	add_property_double(return_value, "Re_w", result.Re_w);
	add_property_double(return_value, "dP", result.dP);
	add_property_double(return_value, "dP_liquid", result.dP_liquid);
	add_property_double(return_value, "F", result.F);
	add_property_double(return_value, "F_gl", result.F_gl);
	add_property_double(return_value, "Volume", result.Volume);
	add_property_double(return_value, "mass", result.mass);
	add_property_long(return_value, "it", result.it);
}

PHP_MINIT_FUNCTION(poverka)
{
	zend_class_entry calc_res;

	INIT_CLASS_ENTRY(calc_res, "Calculation result", NULL);
	calc_res_ptr = zend_register_internal_class(&calc_res);

	return SUCCESS;
}

PHP_MINFO_FUNCTION(poverka)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "poverka support", "enabled");
	php_info_print_table_end();
}

const zend_function_entry poverka_functions[] = {
	PHP_FE(poverka, NULL)
	PHP_FE(confirm_poverka_compiled, NULL)		/* For testing, remove later. */
	PHP_FE_END	/* Must be the last line in poverka_functions[] */
};

zend_module_entry poverka_module_entry = {
	STANDARD_MODULE_HEADER,
	"poverka",
	poverka_functions,
	PHP_MINIT(poverka),
	NULL,
	NULL,
	NULL,
	PHP_MINFO(poverka),
	PHP_POVERKA_VERSION,
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_POVERKA
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
#endif
ZEND_GET_MODULE(poverka)
#endif
