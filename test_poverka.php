<?php
    //-------------------------------------Ввод режимных параметров------------------------------------------------------
    //воздух
    $V_air_out = 1800.0/3600; //объемный расход воздуха на выходе, м^3/сек
    $t_air_in = 25; //температура воздуха на входе, град.Цельсия
    $fi_in = 65; //относительная влажность воздуха на входе, %
    $Rz = 0; //термическое сопротивление загрязнений, м^2*град/Вт, может быть (0 - 12)*10^(-4) м^2*град/Вт
    //если нет других данных, принимают Rz = 2*10^(-4) м^2*град/Вт

    //вода
    $heat_carrier = 1; //тип холодного теплоносителя: 1 - вода, 2 - этиленгликоль, 3 - пропиленгликоль
    $concentration = 100; //концентрация раствора антифриза, %, (нужна, если теплоноситель не вода)
    $tw_in = 7; //температура воды на входе, град.Цельсия
    $tw_out = 12; //температура воды на выходе, град.Цельсия

    //-----------------------------------Ввод конструктивных параметров--------------------------------------------------
    //типоразмер
    $A = 50; //ширина, см  - рабочая длина труб
    $B = 25; //высота, см

    //количество труб
    $n_rows = 3; //количество рядов труб
    $n_front = 10; //количество труб во фронтальном сечении
    $n_1 = 5; //количество труб в одном ходе - количество контуров/количество

    //трубы
    $d = 9.52*pow(10.0,-3); //наружный диаметр труб, мм
    $delta_wall = 0.3*pow(10.0,-3); //толщина стенки труб, мм
    $lambda_wall = 400; //теплопроводность стенки труб, Вт/(м*град), материал стенки - медь 

    //ламели
    $lam_w = 64.96*pow(10.0,-3); //ширина ламелей, м, она же глубина теплообменника
    $Ck = 0.95; //коэффициент учета качества контакта ребра и трубки
    $h_gofr = 1.3*pow(10.0,-3); //амплитуда гофрировки, м
    $delta_f = 0.15*pow(10.0,-3); //толщина ламели, м
    $b_lam = 2.5*pow(10.0,-3); //расстояние между ламелями, м
    $s1 = $B/$n_front*0.01; //поперечный шаг труб, м
    $s2 = 0.02165; //продольный шаг труб, м
    $lambda_f = 200; //теплопроводность ламелей, Вт/(м*град), материал ламелей - алюминий 
    $coef = 1.03; //коэффициент, учитывающий профиль ламели, coef = Fгофр/Fплоская
                // 1 - плоская поверхность
                // 1.03 - 1.05 - треугольный профиль 
                // coef = sqrt((s2/4)^2+h_gofr^2))/(s2/4);

    //коллекторы
    $d_col = 0.025; //внутренний диаметр коллектора, м
    $d_col_ext = 0.0335; //наружний диаметр коллектора, м
    $m_col = 2.39; // кг/м, масса 1 м трубы обыкновенной в соотв. с ГОСТ 3262-75

    //------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------РАСЧЁТ-------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------

    $result = poverka(
        $V_air_out, $t_air_in, $fi_in, $Rz, $tw_in, $tw_out, $A, $B, $n_rows, $n_front, $n_1,
        $d, $delta_wall, $lambda_wall, $lam_w, $Ck, $h_gofr, $delta_f, $b_lam, $s1, $s2, 
        $lambda_f, $coef, $d_col, $d_col_ext, $m_col, $heat_carrier, $concentration
    );
    //------------------------------------------------------------------------------------------------------------

    //Вывод основных параметров и результатов

    echo "RESULTS:\r\n";
    echo "\r\n";
    echo "Dimension type " . $A . " x " . $B ." \r\n";
    echo "\r\n";
    echo "Total heat capacity is " . $result->Q*pow(10.0,-3) . " kW \r\n";
    echo "Explicit heat percent is " . pow($result->dzeta,-1)*100 . " % \r\n";
    echo "\r\n";
    echo "Outlet air volume flow rate is " . $V_air_out*3600 . " m^3/hour\r\n";
    echo "\r\n";
    echo "Inlet air temperature is " . $t_air_in . " degrees Celsius \r\n";
    echo "Outlet air temperature is " . $result->t_air_out . " degrees Celsius \r\n";
    echo "Dew point temperature is " . $result->tp . " degrees Celsius \r\n";
    echo "\r\n";
    echo "Inlet air relative humidity " . $fi_in . " % \r\n";
    echo "Outlet air relative humidity " . $result->fi_out . " % \r\n";
    echo "\r\n";
    echo "liquid mass flow rate equals " . $result->Gw . " kg/sec\r\n";
    echo "\r\n";
    echo "In-tube liquid flow velocity is " . $result->w . " m/s \r\n";
    echo "Flow section air velocity is " . $result->v . " m/s \n";
    echo "\r\n";
    if ($result->Re_w < 2300)
    {
        echo "\r\n";
        echo "In-tube flow mode is LAMINAR Re = " . $result->Re_w . " \r\n";
        echo "\r\n";
        echo "ATTENTION! Incorrect calculation results \r\n";
        echo "\r\n";
    }
    else if (($result->Re_w > 2300) && ($result->Re_w < pow(10.0,4)))
    {
        echo "In-tube flow mode is TRANSIENT Re = " . $result->Re_w . " \r\n";
    }
    else
    { 
        echo "In-tube flow mode is TURBULENT Re = " . $result->Re_w ." \r\n";
    }
    echo "\r\n";
    echo "\r\n";
    echo "AIR pressure drop equals " . $result->dP . " Pa\r\n";
    echo "\r\n";
    echo "liquid pressure drop equals " . $result->dP_liquid*pow(10.0,-3) . " kPa\r\n";
    echo "\r\n";
    echo "\r\n";

    echo "Air side finned heat exchange area " . $result->F . " m^2 \r\n";
    echo "Air side plain heat exchange area " . $result->F_gl . " m^2 \r\n";
    echo "liquid side heat exchange area " . pi()*($d - 2*$delta_wall)*$A*0.01*$n_rows*$n_front . " m^2 \r\n";
    echo "\r\n";
    echo "Filling volume: " . $result->Volume . " litres\r\n";
    echo "Heat exchanger mass: " . $result->mass . " kg \r\n";
    echo "\r\n";
    echo "Number of iterations: " . $result->it . " \r\n";
?>