#define pi 3.14159265358979323846
#define e 2.718281828459046

typedef struct properties //структура для определения теплофизических свойств теплоносителей
{
    double ro; //плотность
    double lambda; //теплопроводность
    double cp; //теплоёмкость
    double mu; //динамическая вязкость
    double nu; //кинематическая вязкость
    double Pr; //число Прандтля

} properties;

typedef struct calc //структура для вывода результатов
{
    double Q; //тепловая мощность
    double dzeta; //коэффициент влаговыпадения
    double t_air_out; //температура воздуха на выходе
    double tp; //температура точки росы
    double fi_out; //относительная влажность воздуха на выходе
    double Gw; //расход охлаждающей воды
    double w; //скорость воды в трубах
    double v; //скорость воздуха 
    double Re_w; //число Рейнольдса со стороны воды
    double dP; //падение давления по воздуху
    double dP_liquid; //падение дваления по воде
    double F; //площадь поврехности теплообмена
    double F_gl; //площадь гладких труб
    double Volume; //заправочный объем
    double mass; //масса аппарата
    int it; //количество итераций, произведённых при поверке
} calc;

struct calc poverka(
    double V_air_out, double t_air_in, double fi_in, double Rz, double tw_in, double tw_out, double A, double B,
    int n_rows, int n_front, int n_1, double d, double delta_wall, double lambda_wall, double lam_w, double Ck,
    double h_gofr, double delta_f, double b_lam, double s1, double s2, double lambda_f, double coef, double d_col,
    double d_col_ext, double m_col, int heat_carrier, double concentration
);
