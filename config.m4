dnl $Id$
dnl config.m4 for extension poverka

PHP_ARG_ENABLE(poverka, whether to enable poverka support,
[  --enable-poverka           Enable poverka support])

if test "$PHP_POVERKA" != "no"; then
  PHP_NEW_EXTENSION(poverka, php_poverka.c poverka.c, $ext_shared)
fi
